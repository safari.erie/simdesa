let accessController = {
	GetKecamatans : SITE_URL+'MgtWilayah/getKecamatan',
	GetDesas : SITE_URL+'MgtWilayah/getDesa',
	Aktivasi : SITE_URL+'MgtWilayah/Aktivasi',
	GetUserByIds : SITE_URL+'User/getUserById',
	SaveUserEdit : SITE_URL+'User/SaveUserEdit',
}


// set combo kec

let GetKecamatan = (kodeKec) => {
	$.ajax({
		url:accessController.GetKecamatans,
		type: "GET",
		dataType: "json",
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=""> Pilih </option>';
				$.each(response.Data,(i,v) => {
					if(kodeKec == v.kode_kec){
						html += `<option value='${v.kode_kec}' selected> ${v.name} </option>`;
					}else{
						html += `<option value='${v.kode_kec}' > ${v.name} </option>`;
					}

					
				});
				$("select[name='kecamatan']").empty();
					$("select[name='kecamatan']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})
}

let GetDesas = (kodeKec,kodeDesa) =>{
	$.ajax({
		url:accessController.GetDesas,
		type: "POST",
		dataType: "json",
		data: {"kode_kec":kodeKec},
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=-1> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {
					if(kodeKec == v.kode_kec){
						html += `<option value='${v.kode_desa}' selected> ${v.name} </option>`;
					}else{
						html += `<option value='${v.kode_desa}' > ${v.name} </option>`;
					}
					
				});
				$("select[name='desa']").empty();
					$("select[name='desa']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})
}


function submit_user(){
	if ($('#user-form').valid()) {
		$('#btn_submit').html('Mohon Tunggu  <i class="fa fa-spinner fa-pulse fa-fw"></i>');
		$('#btn_submit').attr('disabled', 'disabled');
	}
}
