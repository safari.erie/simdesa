
let accessController = {
	Deaktivasi : SITE_URL+'MgtUser/Deaktivasi',
	Aktivasi : SITE_URL+'MgtUser/Aktivasi',
}

var oTables;
$(document).ready(function(){
	
	oTable = $("#table-user").DataTable({		
		"processing": true, // for show progress bar  
        "serverSide": true, // for process server side  
        "lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
        "pagingType": "full_numbers",
		/* "filter": true, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true, */
		/* order: [1, 'desc'], */
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "MgtUser/get_ajax",
			"type": "POST",
			/* "datatype": "json",
			"data": function (d) {      				
				d.nama_pengguna  = $('#nama_pengguna').val();
            } */
        },
        "columnDefs":[
            {
                "targets":[1,3],
                "className":"text-center"
            },
            {
                "targets":[0,-1],
                "orderable":false
            },
          
            
        ]
	   
	});

});

function searchdata(){
	oTable.ajax.reload();
}

function Deaktivasi(idUser){

	swal({
		title: "Konfirmasi?",
		text: "Apakah Yakin User pengguna akan di non aktifkan",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
		.then((willDelete) => {
			
			if (willDelete) {
				let postId = {'id_user':idUser};
				$.ajax({
					url:accessController.Deaktivasi,
					type:'POST',
					data :postId,
					dataType:'json',
					/* beforeSend:function(){
						ProgressBar('wait');
					}, */
					success:function(response){
						//ProgressBar('success');
						if(response.state){
							swal('Deaktivasi Berhasil', 
							{
								icon: "success",
								buttons: false,
								timer: 2000,
								text: response.msg
							});
							
						}else{
							swal('Deaktivasi Gagal', {
								icon: "error",
								buttons: false,
								timer: 4000,
								text: response.msg
							});	
							
						}
						searchdata();
					}

				});
			}
		});

}

function Aktivasi(idUser){
	swal({
		title: "Konfirmasi?",
		text: "Apakah Yakin User pengguna akan di aktifkan",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
		.then((willDelete) => {
			
			if (willDelete) {
				let postId = {'id_user':idUser};
				$.ajax({
					url:accessController.Aktivasi,
					type:'POST',
					data :postId,
					dataType:'json',
					/* beforeSend:function(){
						ProgressBar('wait');
					}, */
					success:function(response){
						//ProgressBar('success');
						if(response.state){
							swal('Aktivasi Berhasil', 
							{
								icon: "success",
								buttons: false,
								timer: 2000,
								text: response.msg
							});
							
						}else{
							swal('Aktivasi Gagal', {
								icon: "error",
								buttons: false,
								timer: 2000,
								text: response.msg
							});	
							
						}
						searchdata();
					}

				});
			}
		});
}