function getBaseURL() {
    var url = location.href;  // entire url including querystring - also: window.location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));

    if (baseURL.indexOf('http://localhost') != -1) {
        // Base Url for localhost
        var url = location.href;  // window.location.href;
        var pathname = location.pathname;  // window.location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index2);

        return baseLocalUrl + "/v2/rcm/";
    } else {
        // Root Url for domain name
        return baseURL + "/";
    }
}

$( document ).ready(function() {
  //profile
  $('#change_image1').click(function() {
    $('#change_image2').trigger('click');
  });

  $('#change_image2').click(function() {
    $('#file_upload').trigger('click');
  });

  $('#file_upload').change(function() {
    $('#submit_upload').trigger('click');
  });

	$('#selectall1').change(function() {
	    var checkboxes = $(this).closest('table').find(':checkbox');
	    checkboxes.prop('checked', $(this).is(':checked'));
	});
	
	$('#selectall2').change(function() {
	    var checkboxes = $(this).closest('table').find(':checkbox');
	    checkboxes.prop('checked', $(this).is(':checked'));
	});

  //delete address and bank_account on profile
	$('.deleteall-profile').click(function(){
		var url    = atob($(this).parent().parent().parent().find('.row').eq(1).find('input[type=hidden]').eq(0).val());
		var alert1 = atob($(this).parent().parent().parent().find('.row').eq(1).find('input[type=hidden]').eq(1).val());
		var alert2 = atob($(this).parent().parent().parent().find('.row').eq(1).find('input[type=hidden]').eq(2).val());
    var alert3 = atob($(this).parent().parent().parent().find('.row').eq(1).find('input[type=hidden]').eq(3).val());

    swal({
      // title: alert1,
      text: alert1,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        var id = [];

        $(this).parent().parent().parent().find('.row').eq(1).find('tbody :checkbox:checked').each(function(i){
          id[i] = $(this).val();
        });

        if ( id.length === 0) {
        
          swal(alert2, {
            icon: "error",
            buttons: false,
            timer: 2000,
          });
        
        } else {
        
          $.ajax({
            url:getBaseURL() + url,
            method:'POST',
            data:{id:id},
            success:function() {
              for ( var i = 0; i < id.length; i++ ) {
                $('tr#id-'+id[i]+'').css('background-color', '#ccc');
                $('tr#id-'+id[i]+'').fadeOut('slow');
              }

              swal(alert3, {
                icon: "success",
                buttons: false,
                timer: 2000,
              });
              setTimeout(function() {
                  location.reload(true);
              }, 2000);

            }
          });
        }
      }
    });
	});

});