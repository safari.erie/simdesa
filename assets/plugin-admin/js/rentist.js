
$(document).ready(function() {

  function sndx(tl, fm, url = "", id_asset = "", type = "") {
      var tl;
      var fm;
      swal({
        text: "Loading ...",
        icon: "info",
        button: false,
      });
      $.ajax({
          url: "localhost/json/",
          type: "post",
          data: $(fm).serialize(),
          dataType: "json",
          xhrFields: {
              withCredentials: true
          },
          crossDomain: true,
          cache: false,
          success: function(result) {
              if (result.status) {
                  swal({
                      title: tl,
                      text: result.message,
                      icon: "success",
                      button: false,
                      timer: 2000,
                  });
                  setTimeout(function() {
                      if (id_asset != "") {
                        if (type == "add") {
                          window.location = '/' + id_asset + '/' + result.data.id_asset;
                        } else {
                          window.location = '/' + id_asset;
                        }
                      } else {
                        if(url == ""){
                          location.reload(true);    
                        } else {
                          window.location = url;    
                        }
                      }
                      
                  }, 2000);
              } else {
                swal({
                    title: "Error!",
                    text: result.message,
                    icon: "error",
                    button: false,
                    timer: 2000,
                });
              }
          },
          error: function(xhr, Status, err) {
              swal({
                  title: "Error! (" + Status + ")",
                  text: "Terjadi kesalahan",
                  icon: "error",
                  button: false,
                  timer: 2000,
              });
              console.log(xhr);
              console.log(Status);
              console.log(err);
          }
      });
      return false;
  }

  function upl(tl, fm, url = "", id_asset = "", type = "") {
      var tl;
      var fm;
      swal({
        text: "Loading ...",
        icon: "info",
        button: false,
      });
      $.ajax({
          url: "localhost/json/",
          type: "post",
          data: new FormData(fm),
          contentType: false,
          cache: false,
          processData: false,
          dataType: "json",
          xhrFields: {
              withCredentials: true
          },
          crossDomain: true,
          cache: false,
          success: function(result) {
              if (result.status) {
                  swal({
                      title: tl,
                      text: result.message,
                      icon: "success",
                      button: false,
                      timer: 2000,
                  });
                  setTimeout(function() {
                      
                      if (id_asset != "") {
                        if (type == "add") {
                          window.location = '/' + id_asset + '/' + result.data.id_asset;
                        } else {
                          window.location = '/' + id_asset;
                        }
                      } else {
                        if(url == ""){
                          location.reload(true);    
                        } else {
                          window.location = url;    
                        }
                      }

                  }, 2000);
              } else {
                  swal({
                      title: "Error!",
                      text: result.message,
                      icon: "error",
                      button: false,
                      timer: 2000,
                  });
              }
          },
          error: function(xhr, Status, err) {
              swal({
                  title: "Error! (" + Status + ")",
                  text: "Terjadi kesalahan",
                  icon: "error",
                  button: false,
                  timer: 2000,
              });
              setTimeout(function() {
                  location.reload(true);
              }, 2000);
          }
      });
      return false;
  }

  $("#rent-form").submit(function() {
      var form = $(this);
      var title = form.data('title');
      var url = form.data('url');
      var asset = form.data('asset');
      var type = form.data('type');
      sndx(title, form, url, asset, type);
      return false;
  });

  $("#rent-form2").submit(function() {
      var form = $(this);
      var title = form.data('title');
      var url = form.data('url');
      var asset = form.data('asset');
      var type = form.data('type');
      sndx(title, form, url, asset, type);
      return false;
  });
  
  $("#msform").submit(function() {
      var form = $(this);
      var title = form.data('title');
      var url = form.data('url');
      var asset = form.data('asset');
      var type = form.data('type');
      sndx(title, form, url, asset, type);
      return false;
  });

  $("#rent-upload").submit(function() {
      var form = $(this);
      var title = form.data('title');
      var url = form.data('url');
      var asset = form.data('asset');
      var type = form.data('type');
      upl(title, this, url, asset, type);
      return false;
  });

  $("#rent-upload2").submit(function() {
      var form = $(this);
      var title = form.data('title');
      var url = form.data('url');
      var asset = form.data('asset');
      var type = form.data('type');
      upl(title, this, url, asset, type);
      return false;
  });

});