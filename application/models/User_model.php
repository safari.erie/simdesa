<?php
/*
* @author: safari.erie@gmail.com
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

	public function __construct()
	{
        parent::__construct();
        $this->load->model('common_model','cmnm');
	}



	 // start datatables
	 var $column_order = array(null, 'username', 'email', 'name', 'role_name', 'status'); //set column field database for datatable orderable
	 var $column_search = array('username', 'email', 'role_name'); //set column field database for datatable searchable
	 var $order = array('id_user' => 'asc'); // default order 
  
	 private function _get_datatables_query() {
		 $this->db->select('a.id_user,a.username,a.password,a.email,a.status');
		 $this->db->select("b.first_name,b.middle_name,b.last_name, concat(b.first_name,'',b.middle_name,' ',b.last_name) as name,b.address");
		 $this->db->select('b.phone_number,b.mobile_number,b.kode_wilayah,d.role_name');
		 $this->db->from('tb_user a');
		 $this->db->join('tb_user_profile b', 'a.id_user = b.id_user');
		 $this->db->join('tb_user_role c', 'a.id_user = c.id_user');
		 $this->db->join('tb_role d', 'c.id_role = d.id_role');
		 $i = 0;
		 foreach ($this->column_search as $item) { // loop column 
			 if(@$_POST['search']['value']) { // if datatable send POST for search
				 if($i===0) { // first loop
					 $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					 $this->db->like($item, $_POST['search']['value']);
				 } else {
					 $this->db->or_like($item, $_POST['search']['value']);
				 }
				 if(count($this->column_search) - 1 == $i) //last loop
					 $this->db->group_end(); //close bracket
			 }
			 $i++;
		 }
		  
		 if(isset($_POST['order'])) { // here order processing
			 $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		 }  else if(isset($this->order)) {
			 $order = $this->order;
			 $this->db->order_by(key($order), $order[key($order)]);
		 }
	 }
	 function get_datatables() {
		 $this->_get_datatables_query();
		 if(@$_POST['length'] != -1)
		 $this->db->limit(@$_POST['length'], @$_POST['start']);
		 $query = $this->db->get();
		 return $query->result();
	 }
	 function count_filtered() {
		 $this->_get_datatables_query();
		 $query = $this->db->get();
		 return $query->num_rows();
	 }
	 function count_all() {
		 $this->db->from('tb_user');
		 return $this->db->count_all_results();
	 }
	 // end datatables

    public function GetRole(){

		$sql = "
			SELECT 
			id_role,role_name 
			FROM public.tb_role
		";

		$query = $this->db->query($sql);
		return $query->result();
	}

	public function checkUserNameOrEmail($username,$email){
		$username_lower = strtolower($username);
		$email_lower = strtolower($email);
		$sql = "
			select id_user,username,email from tb_user
			where LOWER(username) = '$username_lower' or LOWER(email) = '$email_lower'
		";

		$query = $this->db->query($sql);
		return $query->row();

	}

	public function save_user($data_user){

		$getLastId = $this->cmnm->common_last_id('id_user','tb_user');
		$lastIdUser;
		if($getLastId->lastid == null){
			$lastIdUser = 1;
		}else{
			$lastIdUser = $getLastId->lastid + 1;
		}
		$dt_tbl_user = array(
			'id_user' => $lastIdUser,
			'username'	=> $data_user['username'],
			'password'	=> $data_user['password'],
			'email'		=> $data_user['email'],
			'status'	=> 1,
			'created_date' => date('Y-m-d H:i:s')
		);
		$inserUser = $this->db->insert('tb_user',$dt_tbl_user);

		$dt_tb_user_info = array(
			'id_user'	=> $lastIdUser,
			'first_name'	=> $data_user['firstName'],
			'middle_name'	=> $data_user['middleName'],
			'last_name'	=> $data_user['lastName'],
			'address'	=> $data_user['address'],
			'phone_number'	=> $data_user['phoneNumber'],
			'mobile_number'	=> $data_user['mobileNumber'],
			'kode_wilayah'	=> $data_user['kodeWilayah'],
		);

		$insertUserProfile = $this->db->insert('tb_user_profile', $dt_tb_user_info);

		$dt_user_role = array(
			'id_user' => $lastIdUser,
			'id_role' => $data_user['roles']
		);

		$insertUserRole = $this->db->insert('tb_user_role', $dt_user_role);
		$user_detail = array(
            'last_user' => $lastIdUser
        );

        return $user_detail; 
	}

	function update_user($id, $data_user){
		$this->db->trans_start();
		$dt_tbl_user = array(
			'username'	=> $data_user['username'],
			'email'		=> $data_user['email'],
			'updated_by' => $this->session->userdata(S_ID_USER),
			'updated_date' => date('Y-m-d H:i:s'),
		);

		$dt_tb_user_info = array(
			'first_name'	=> $data_user['firstName'],
			'middle_name'	=> $data_user['middleName'],
			'last_name'		=> $data_user['lastName'],
			'address'		=> $data_user['address'],
			'phone_number'	=> $data_user['phoneNumber'],
			'mobile_number'	=> $data_user['mobileNumber'],
			'kode_wilayah'	=> $data_user['kodeWilayah'],
		);

		$dt_user_role = array(
			'id_role' => $data_user['roles']
		);

		$this->db->where('id_user',$id);
		$this->db->update('public.tb_user',$dt_tbl_user);

		$this->db->where('id_user',$id);
		$this->db->update('public.tb_user_profile',$dt_tb_user_info);

		$this->db->where('id_user',$id);
		$this->db->update('public.tb_user_role',$dt_user_role);

		$this->db->trans_complete();

		$result;		
		if($this->db->trans_status() === FALSE){ // Check if transaction result successful
			$result = false;
		}else{
			$result = true;
		}
		return $result;

	}


	public function getUserById($idUser){
		$sql = "
			select a.id_user,a.username,a.email,
					b.first_name,b.middle_name,b.last_name,b.address,b.phone_number,b.mobile_number,b.kode_wilayah,
					d.id_role,d.role_name
			from tb_user a
			inner join tb_user_profile b on a.id_user = b.id_user
			inner join tb_user_role c on a.id_user = c.id_user
			inner join tb_role d on c.id_role = d.id_role
			where a.id_user = '{$idUser}'";

		$query = $this->db->query($sql)->row();
		return $query;
	}

	public function AktivasiDeaktivasi($idUser, $data){
		$this->db->where('id_user',$idUser);
		$this->db->update('public.tb_user', $data);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}
		return $result;

	}






}

?>