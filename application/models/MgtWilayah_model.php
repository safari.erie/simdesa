<?php


class MgtWilayah_model extends CI_Model
{
    
	public function getKec(){

		$sql = "
			Select 
				kode_kec,name FROM 
				master.tb_kec
		";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getDesa($kode_kec){
		$sql = "
			SELECT a.kode_desa, a.kode_kec,a.name, b.name as name_kecamatan
			FROM master.tb_desa a
			JOIN master.tb_kec b on  a.kode_kec = b.kode_kec
			where a.kode_kec = $kode_kec
		";
		
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getDesaAll(){

		$sql = "
			SELECT a.kode_desa, a.kode_kec,a.name
			FROM master.tb_desa a
		";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getBantuan(){
		$sql = "
			SELECT id_bantuan,nama_bantuan from ref.tb_bantuan;
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}


	function getDataKecamatans(
		$start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
		, $namaKec = ''
	){
		$sql = "
		SELECT kode_kec,name as nama_kecamatan
		FROM master.tb_kec";

		if($namaKec != ''){
			$sql .= " AND name LIKE '{$namaKec}'";
		}

		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();
	}

	function count_data_kecamatan($namaKec){
		$sql = "
		SELECT count(*) as cnt
		FROM master.tb_kec";

		if($namaKec != ''){
			$sql .= " AND name LIKE '{$namaKec}'";
		}

		return $this->db->query($sql)->row()->cnt;
	}


	function getDatadesas(
		$start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
		, $namaDesa = ''
	){
		$sql = "					
			SELECT a.kode_desa,a.name as nama_desa,
			b.kode_kec,b.name as nama_kecamatan
			FROM master.tb_desa a
			inner join master.tb_kec b on a.kode_kec = b.kode_kec
		";

		if($namaDesa != ''){
			$sql .= " AND a.name LIKE '{$namaDesa}'";
		}

		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();
	}

	function count_data_desa($namaDesa){
		$sql = "
		SELECT count(*) as cnt
		FROM master.tb_desa a
		inner join master.tb_kec b on a.kode_kec = b.kode_kec
		
		";

		if($namaDesa != ''){
			$sql .= " AND a.name LIKE '{$namaDesa}'";
		}

		return $this->db->query($sql)->row()->cnt;
	}

	function SaveKecamatan($id_kecamatan,$data_insert,$tableName){

		if($id_kecamatan == -1){

			$this->db->insert($tableName, $data_insert);
			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;
		}else{
			$this->db->where('kode_kec',$id_kecamatan);
			$this->db->update($tableName,$data_insert);
			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;
		}
	}

	function saveDesa($id_desa, $data, $tableName){
		if($id_desa == -1){
			$this->db->insert($tableName, $data);
			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;
		}
	}

	function GetDataKecByKode($kodeKec){
		$sql = "
			SELECT 
				kode_kec,kode_kab,name 
			FROM master.tb_kec where kode_kec = $kodeKec
		";

		$query = $this->db->query($sql);
		return $query->row();


	}

}
