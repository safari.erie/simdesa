<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('alert') ) {
	function alert() {
		$ci =& get_instance();		
		if ( $ci->session->flashdata('success') ) {
			echo "<div class='alert alert-success alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('success');
			echo "</div>";
		} elseif ( $ci->session->flashdata('info') ) {
			echo "<div class='alert alert-info alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('info');
			echo "</div>";
        } elseif ( $ci->session->flashdata('warning') ) {
			echo "<div class='alert alert-warning alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('warning');
			echo "</div>";
		} elseif ( $ci->session->flashdata('danger') ) {
			echo "<div class='alert alert-danger alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('danger');
			echo "</div>";
		}
	}
}

?>