<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MgtDesa extends CI_Controller {

    var $url   			 = 'mgtdesa';
	var $model 			 = 'Model_user';
	var $model_wilayah = 'Model_wilayah';
    var $model_desa 	 = 'Model_desa';

    public function __construct()
	{
        parent::__construct();
        $this->load->model('user_model','usm');
    }
    
    function index(){
        $assets = array(
            "title_page" => "Master Data > List " . $this->url
        );
		$this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/list_' . $this->url);	
		$this->load->view('admins/templates/home/footer', $assets);
    }

    function add(){
        $assets = array(
            "title_page" => "Master Data > Add " . $this->url
		);
		
		$data = array();
        $this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/add_' . $this->url);	
		$this->load->view('admins/templates/home/footer', $assets);
    }

}

?>