<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MgtRab extends CI_Controller {
    
    var $url   			 = 'mgtrab';
	var $model 			 = 'Model_Rab';
	var $model_wilayah   = 'Model_wilayah';
    var $model_desa 	 = 'Model_desa';


    public function __construct()
	{
        parent::__construct();
        $this->load->model('rab_model','rabm');
    }


    function index(){
        $assets = array(
            "title_page" => "Master Monitoring > List " . $this->url
        );
		$this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/list_' . $this->url);	
        $this->load->view('admins/templates/home/footer', $assets);

    }

    function add(){
        $assets = array(
            "title_page" => "Master Monitoring > Add " . $this->url
        );
        $data['js'] = array('assets/jsapp/master_rab/addMasterRab');
		$this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/add_' . $this->url);	
        $this->load->view('admins/templates/home/footer', $data);

    }

    function getDataProposal(){
        $sampleProposal = array(
            'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
    }

}