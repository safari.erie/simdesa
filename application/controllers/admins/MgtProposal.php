<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MgtProposal extends CI_Controller {

    var $url   			 = 'mgtproposal';
	var $model 			 = 'Model_user';
	var $model_wilayah = 'Model_wilayah';
    var $model_desa 	 = 'Model_desa';

    public function __construct()
	{
        parent::__construct();
        $this->load->model('proposal_model','prom');
    }

    public function index(){
        $assets = array(
            "title_page" => "Master Monitoring > List " . $this->url
        );
		$this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/list_' . $this->url);	
		$this->load->view('admins/templates/home/footer', $assets);
    }

    public function add(){
        $assets = array(
            "title_page" => "Master Monitoring > Add " . $this->url
		);
        $this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/add_' . $this->url);	
		$this->load->view('admins/templates/home/footer', $assets);
    }

}