<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sign extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('sign_model','sgnm');
	}


	public function index()
	{
		$asset = array(
			'title' => 'Login',
			"js"         => array('assets/jsapp/sign/sign')   
		 );

		$post = $this->input->post();

		$this->load->view('admins/templates/login/header',$asset);
		$this->load->view('admins/sign/index');
		$this->load->view('admins/templates/login/footer',$asset);
	}


	public function check_login(){

		$usernameOremail = $this->input->post('username');
		$password = md5($this->input->post('password',true));
		
		$get_user_login = $this->sgnm->auth_user($usernameOremail);
		
		if(count($get_user_login) > 0){
			$get_user_login = $get_user_login[0];			
			if($password == $get_user_login->password){
				$this->session->set_userdata(S_USER_NAME, $get_user_login->username);
                $this->session->set_userdata(S_USER_EMAIL, $get_user_login->email);
                $this->session->set_userdata(S_LOGGED_IN, TRUE);
				$this->session->set_userdata(S_ID_USER,$get_user_login->id_user);
				redirect('admins/Home');
			}else{
				$this->session->set_flashdata('notif_pass_wrong', '<strong>Gagal. </strong> Password anda tidak sesuai.');
                redirect('admins/Sign/failed');
			}

		}else{
			$this->session->set_flashdata('notif_email_wrong', '<strong>Gagal. </strong> Username atau Email anda tidak terdaftar.');
            redirect('admins/Sign/');
		}

	}

	function failed() {        
        $this->index();
	}
	
	function out(){
		$this->session->sess_destroy();
        redirect('admins/Sign/' . $this->uri->segment(4));
	}
}
