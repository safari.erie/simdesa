<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MgtUser extends CI_Controller {

	
    var $url   			 = 'mgtuser';    
	public function __construct()
	{
        parent::__construct();
        $this->lang->load('information_lang');
        $this->load->model('user_model','usm');
        $this->load->model('common_model','cmnm');
        $this->load->model('MgtWilayah_model','wm');
    }
    

	public function index()
	{
		
        $assets = array(
            "title_page" => "Master Data > List " . $this->url,
            "js"         => array('assets/jsapp/master_user/master_user_list') 
		);
		$this->load->view('admins/templates/home/header', $assets);		
        $this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/'. $this->url . '/list_' . $this->url);	
		$this->load->view('admins/templates/home/footer', $assets);	
    }
    
    function add(){

        $data['role'] = $this->usm->GetRole();
        $data = array(
			"list_kecamatan" => $this->wm->getKec(),
			"list_desa" => $this->wm->getDesaAll(),
			"role" => $this->usm->GetRole() 
		);
        $assets = array(
            "title_page" => "Master Data > Add " . $this->url,
            "js"         => array('assets/jsapp/master_user/master_user_create')   
        );
        
        $post = $this->input->post();
		$data['post'] = $post;
        if (isset($post['save']) ) {
            $checkUser = $this->usm->checkUserNameOrEmail($post['username'],$post['email']);
          
            if(count($checkUser) != 0){
                $this->session->set_flashdata('warning', 'Gagal meyimpan data. <strong> username atau email sudah terdaftar </strong>');
            }else{
                $data_post = array();                
                $roles = $post['role'];
                if($roles == 1 || $roles == 2){
                    $data_post = array(
                        'username' => $post['username'],
                        'password' => md5($post['password']),
                        'email'	   => $post['email'],
                        'firstName' => $post['firstName'],
                        'middleName' => $post['middleName'],
                        'lastName'	=> $post['lastName'],
                        'address'	=> $post['address'],
                        'phoneNumber'	=> $post['PhoneNumber'],
                        'mobileNumber'	=> $post['MobileNumber'],
                        'roles'	=> $roles,
                        'kodeWilayah' => 0,
                    );
                }else if($roles == 3){
                    $data_post = array(
                        'username' => $post['username'],
                        'password' => md5($post['password']),
                        'email'	   => $post['email'],
                        'firstName' => $post['firstName'],
                        'middleName' => $post['middleName'],
                        'lastName'	=> $post['lastName'],
                        'address'	=> $post['address'],
                        'phoneNumber'	=> $post['PhoneNumber'],
                        'mobileNumber'	=> $post['MobileNumber'],
                        'roles'	=> $roles,
                        'kodeWilayah' => $post['kecamatan'],
                    );
                }else if($roles == 4){
                    $data_post = array(
                        'username' => $post['username'],
                        'password' => md5($post['password']),
                        'email'	   => $post['email'],
                        'firstName' => $post['firstName'],
                        'middleName' => $post['middleName'],
                        'lastName'	=> $post['lastName'],
                        'address'	=> $post['address'],
                        'phoneNumber'	=> $post['PhoneNumber'],
                        'mobileNumber'	=> $post['MobileNumber'],
                        'roles'	=> $roles,
                        'kodeWilayah' => $post['desa'],
                    );
                }
                $save = $this->usm->save_user($data_post);
                if ( $save ) {
                    $this->session->set_flashdata('success', 'Berhasil menyimpan data.');
                } else {
                    $this->session->set_flashdata('warning', 'Gagal meyimpan data.');
                }
            }
            
		}
        $this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/add_' . $this->url,$data);	
		$this->load->view('admins/templates/home/footer', $assets);
    }

    function get_ajax() {
        $list = $this->usm->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $htmlButton ='';
            $htmlTitleStatus = '';
            if($item->status == 1){
                $htmlButton .= '<button type="button" class="btn btn-danger btn-flat btn-sm" onclick="Deaktivasi('.$item->id_user.');"><i class="fa fa-times"></i> Nonaktif </button>  ';
                $htmlTitleStatus .= 'Aktif';
            }else{
                $htmlButton .= '<button type="button" class="btn btn-warning btn-flat btn-sm" onclick="Aktivasi('.$item->id_user.');"><i class="fa fa-check"></i> Aktivasi</button> ';
                $htmlTitleStatus .= 'Tidak Aktif';
            }
            $retButton = "";
            $retButton .= $htmlButton.' <a class="btn btn-info btn-flat btn-sm" href='.base_url().'admins/MgtUser/Views?ids='.$item->id_user.'> <i class="fa fa-edit"></i> View </a>'; 
            $row = array();
            $row[] = $no.".";            
            $row[] = $item->name;
            $row[] = $item->username;
            $row[] = $item->email;
            $row[] = $item->role_name;
            $row[] = $htmlTitleStatus;
            $row[] = $retButton;            
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->usm->count_all(),
            "recordsFiltered" => $this->usm->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    function Views(){
        $ids = $this->input->get('ids');       
        
        $assets = array(
            "title_page" => "Master Data Pengguna > View " . $this->url,
            "js"         => array('assets/jsapp/master_user/master_user_view') 
        );

        $post = $this->input->post();
        if (isset($post['save']) ) {
            
			unset($post['save']);
            $checkUser = $this->usm->checkUserNameOrEmail($post['username'],$post['email']);
            if(count($checkUser) != 0){
                
                if($checkUser->id_user != $ids){
                    $this->session->set_flashdata('warning', 'Gagal meyimpan data. <strong> username atau email sudah terdaftar </strong>');
                }else{
                    $data_post = array();                
                    $roles = $post['role'];
                    if($roles == 1 || $roles == 2){
                        $data_post = array(
                            'username' => $post['username'],
                            'email'	   => $post['email'],
                            'firstName' => $post['first_name'],
                            'middleName' => $post['middle_name'],
                            'lastName'	=> $post['last_name'],
                            'address'	=> $post['address'],
                            'phoneNumber'	=> $post['phone_number'],
                            'mobileNumber'	=> $post['mobile_number'],
                            'roles'	=> $roles,
                            'kodeWilayah' => 0,
                        );
                    }else if($roles == 3){
                        $data_post = array(
                            'username' => $post['username'],
                            'email'	   => $post['email'],
                            'firstName' => $post['first_name'],
                            'middleName' => $post['middle_name'],
                            'lastName'	=> $post['last_name'],
                            'address'	=> $post['address'],
                            'phoneNumber'	=> $post['phone_number'],
                            'mobileNumber'	=> $post['mobile_number'],
                            'roles'	=> $roles,
                            'kodeWilayah' => $post['kecamatan'],
                        );
                    }else if($roles == 4){
                        $data_post = array(
                            'username' => $post['username'],
                            'email'	   => $post['email'],
                            'firstName' => $post['first_name'],
                            'middleName' => $post['middle_name'],
                            'lastName'	=> $post['last_name'],
                            'address'	=> $post['address'],
                            'phoneNumber'	=> $post['phone_number'],
                            'mobileNumber'	=> $post['mobile_number'],
                            'roles'	=> $roles,
                            'kodeWilayah' => $post['desa'],
                        );
                    }
                   
                    $save = $this->usm->update_user($ids,$data_post);
                    if ( $save ) {
                        $this->session->set_flashdata('success', 'Berhasil merubah data.');
                        $user = $this->usm->getUserById($ids);     
                        if ($user) {
                            $post['nama'] 	  = $user->username;
                            $post['username'] = $user->username;
                            $post['role'] 	  = $user->id_role;
                            $post['email'] 	  = $user->email;
                            $post['first_name'] 	  = $user->first_name;
                            $post['middle_name'] 	  = $user->middle_name;
                            $post['last_name'] 	      = $user->last_name;
                            $post['address'] 	      = $user->address;
                            $post['phone_number'] 	      = $user->phone_number;
                            $post['mobile_number'] 	      = $user->mobile_number;
                            if ($post['role'] == 3) {
                                $post['kode_kec'] = $user->kode_wilayah;
                            } elseif ($post['role'] == 4) {
                                $post['kode_desa'] = $user->kode_wilayah;
                            }
                        }
                    } else {
                        $this->session->set_flashdata('warning', 'Gagal merubah data.');
                    }
                }

            }else{
                    $data_post = array();                
                    $roles = $post['role'];
                    if($roles == 1 || $roles == 2){
                        $data_post = array(
                            'username' => $post['username'],
                            'email'	   => $post['email'],
                            'firstName' => $post['first_name'],
                            'middleName' => $post['middle_name'],
                            'lastName'	=> $post['last_name'],
                            'address'	=> $post['address'],
                            'phoneNumber'	=> $post['phone_number'],
                            'mobileNumber'	=> $post['mobile_number'],
                            'roles'	=> $roles,
                            'kodeWilayah' => 0,
                        );
                    }else if($roles == 3){
                        $data_post = array(
                            'username' => $post['username'],
                            'email'	   => $post['email'],
                            'firstName' => $post['first_name'],
                            'middleName' => $post['middle_name'],
                            'lastName'	=> $post['last_name'],
                            'address'	=> $post['address'],
                            'phoneNumber'	=> $post['phone_number'],
                            'mobileNumber'	=> $post['mobile_number'],
                            'roles'	=> $roles,
                            'kodeWilayah' => $post['kecamatan'],
                        );
                    }else if($roles == 4){
                        $data_post = array(
                            'username' => $post['username'],
                            'email'	   => $post['email'],
                            'firstName' => $post['first_name'],
                            'middleName' => $post['middle_name'],
                            'lastName'	=> $post['last_name'],
                            'address'	=> $post['address'],
                            'phoneNumber'	=> $post['phone_number'],
                            'mobileNumber'	=> $post['mobile_number'],
                            'roles'	=> $roles,
                            'kodeWilayah' => $post['desa'],
                        );
                    }
                    $save = $this->usm->update_user($ids,$data_post);
                    if ( $save ) {
                        $this->session->set_flashdata('success', 'Berhasil merubah data.');
                        $user = $this->usm->getUserById($ids);        
                        if ($user) {
                            $post['nama'] 	  = $user->username;
                            $post['username'] = $user->username;
                            $post['role'] 	  = $user->id_role;
                            $post['email'] 	  = $user->email;
                            $post['first_name'] 	  = $user->first_name;
                            $post['middle_name'] 	  = $user->middle_name;
                            $post['last_name'] 	      = $user->last_name;
                            $post['address'] 	      = $user->address;
                            $post['phone_number'] 	      = $user->phone_number;
                            $post['mobile_number'] 	      = $user->mobile_number;
                            if ($post['role'] == 3) {
                                $post['kode_kec'] = $user->kode_kec;
                            } elseif ($post['role'] == 4) {
                                $post['kode_desa'] = $user->kode_wilayah;
                            }
                        }
                    } else {
                        $this->session->set_flashdata('warning', 'Gagal meyimpan data.');
                    }
            }
            
                
		}

        if($post){
            $post = $post;
           
        }else{
            $user = $this->usm->getUserById($ids);        
            if ($user) {
                $post['nama'] 	  = $user->username;
                $post['username'] = $user->username;
                $post['role'] 	  = $user->id_role;
                $post['email'] 	  = $user->email;
                $post['first_name'] 	  = $user->first_name;
                $post['middle_name'] 	  = $user->middle_name;
                $post['last_name'] 	      = $user->last_name;
                $post['address'] 	      = $user->address;
                $post['phone_number'] 	      = $user->phone_number;
                $post['mobile_number'] 	      = $user->mobile_number;
                if ($post['role'] == 3) {
                    $post['kode_kec'] = $user->kode_wilayah;
                } elseif ($post['role'] == 4) {
                    $post['kode_desa'] = $user->kode_wilayah;
                }
            }
        }
       

        $data = array(
			"list_kecamatan" => $this->wm->getKec(),
			"list_desa" => $this->wm->getDesaAll(),
            "role" => $this->usm->GetRole(),
            "post" => $post,
        );

        $this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/' . $this->url . '/view_' . $this->url,$data);	
		$this->load->view('admins/templates/home/footer', $assets);

       
    }


    function Deaktivasi(){
        $id_user = $this->input->post('id_user');

		$data_update = array(
			'status'	=> 0,
			'updated_by'	=> $this->session->userdata(S_ID_USER),
			'updated_date' => date('Y-m-d H:i:s')
		);

		$updateDeaktivasi = $this->usm->AktivasiDeaktivasi($id_user, $data_update);
		if($updateDeaktivasi){
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berhasil di Dekatif',
				'data'	=> null,
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data Gagal Dekatif',
				'data'	=> null,
			);
			echo json_encode($output);
		}
    }


    function Aktivasi(){
		$id_user = $this->input->post('id_user');

		$data_update = array(
			'status'	=> 1,
			'updated_by'	=> $this->session->userdata(S_ID_USER),
			'updated_date' => date('Y-m-d H:i:s')
		);

		$updateDeaktivasi = $this->usm->AktivasiDeaktivasi($id_user, $data_update);
		if($updateDeaktivasi){
			$output = array(
				'state'	=> true,
				'msg'	=> 'Data Berhasil di Aktivasi',
				'data'	=> null,
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data Gagal Aktivasi',
				'data'	=> null,
			);
			echo json_encode($output);
		}
	}



	
}
