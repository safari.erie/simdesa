<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends CI_Controller {

	var $url   = 'home';
	var $model = 'Model_home';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
        $assets = array();

		$this->load->view('admins/templates/home/header', $assets);		
		$this->load->view('admins/templates/home/menu');		
		$this->load->view('admins/home/index');	
		$this->load->view('admins/templates/home/footer', $assets);	
	}



	
}
