<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MgtWilayah extends CI_Controller {

	function __construct() {
        parent:: __construct();
		$this->load->model('MgtWilayah_model','wm');		
	}


	function getKecamatan()
	{

		$data = $this->wm->getKec();
		$result['Data'] = $data;
		echo json_encode($result);
	}

	function getDesa(){
		$kode_kec = $this->input->post('kode_kec');
		
		$data = $this->wm->getDesa($kode_kec);
		$result['Data'] = $data;
		echo json_encode($result);

	}




}
