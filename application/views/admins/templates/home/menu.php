<header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>rcmadmin/home">
                        <b>
                            S
                        </b>
                        <span style="align:center">
                            | Monitoring Desa 2020
                        </span>
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> 
                            <a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark" href="javascript:void(0)">
                                <i class="ti-menu"></i>
                            </a>
                        </li>
                        <li class="nav-item"> 
                            <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)">
                                <i class="icon-menu"></i>
                            </a> 
                        </li>
                    </ul>

                    <ul class="navbar-nav my-lg-0">

                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="hidden-md-down" style="color:black;">
                                    
                                    <strong> <?php echo $this->session->userdata(S_USER_NAME);?>  </strong> 
                                    
                                    <i class="icon-options-vertical"></i>
                                </span>
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <!-- <a href="<?php //echo base_url(); ?>rcmadmin/auth/change_password" class="dropdown-item"><i class="ti-user"></i> 
                                    Ubah Kata Sandi
                                </a> -->
                                
                                <div class="dropdown-divider"></div>
                                <a href="<?php echo base_url(); ?>admins/Sign/out" class="dropdown-item"><i class="fa fa-power-off"></i>
                                    Keluar
                                </a>
                            </div>
                        </li>
                    </ul>

                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    

                    <ul id="sidebarnav">
                    <?php
                        
                        $ci =&get_instance();
                        $ci->load->model('Menu_model');
                        $menu = $ci->Menu_model->show_menu(); 
                        echo $menu;
                        ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

        <div class="page-wrapper">
            <div class="container-fluid">
                