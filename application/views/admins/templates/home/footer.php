</div>
        </div>
        <footer class="footer">
            © 2020 Simdesa
        </footer>
    </div>


    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScripassets/plugin-admin/t -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/custom.min.js"></script>
    <!--Custom select -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/select2.min.js"></script>
    <!--Data Tables -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/jquery.dataTables.min.js"></script>
    <!--Fancy Box -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/fancybox/jquery.fancybox.min.js"></script>
    <!--Fancy Box -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/toast/jquery.toast.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- <script src="<?php //echo base_url() ?>js/rentist.js"></script> -->
    <script src="<?php echo base_url() ?>assets/plugin-admin/js/global.js"></script>
    <script src="<?php echo base_url() ?>assets/plugin-admin/js/phone/intlTelInput.js"></script>
    <script>
         $(function() {
            $(".preloader").fadeOut();
        });
        var SITE_URL = '<?php echo site_url() ?>admins/';
    </script>
    <!-- Add js on controller -->
    <?php
        if( ! empty( $js ) ) { 
            foreach ($js as $linkjs) echo '<script src="' . base_url() . $linkjs . '.js" /></script>', "\n"; 
        }
    ?>
    
    <script src="<?php echo base_url() ?>assets/plugin-admin/js/tool.js"></script>

    <script src="<?php echo base_url() ?>assets/plugin-admin/js/datepicker/moment.js"></script>

   
</body>

</html>
