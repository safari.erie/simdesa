<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="<?php //echo base_url(); ?>images/rentist-logo-icon.png"> -->
    <title>Monitoring Desa</title>
    <link href="<?php echo base_url(); ?>assets/plugin-admin/css/home/style.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugin-admin/css/home/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugin-admin/css/fancybox/jquery.fancybox.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugin-admin/css/toast/jquery.toast.css" rel="stylesheet" />

    <?php
        if( ! empty( $css ) ) { 
            foreach ($css as $style) echo '<link rel="stylesheet" type="text/css" href="' . base_url() . $style . '.css" />', "\n"; 
        }
    ?>

    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/home/jquery-3.2.1.min.js"></script>
</head>

<body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Monitoring Desa</p>
        </div>
    </div>
    <div id="main-wrapper">
        