    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/login/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugin-admin/js/bootstrap.min.js"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- <script src="<?php //echo base_url() ?>js/rentist.js"></script> -->
    <script src="<?php echo base_url() ?>assets/plugin-admin/js/global.js"></script>
    <script src="<?php echo base_url() ?>assets/plugin-admin/js/phone/intlTelInput.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#rent-form").slideUp();
            $("#msform").fadeIn();
        });

        $('#close').on("click", function() {
            $("#msform").fadeOut();
            $("#rent-form").slideDown();
        });
    </script>
    <!-- Add js on controller -->
    <?php
        if( ! empty( $js ) ) { 
            foreach ($js as $linkjs) echo '<script src="' . base_url() . $linkjs . '.js" /></script>', "\n"; 
        }
    ?>
    
</body>

</html>