<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                List User
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>admins/MgtUser/Add">
                Add User
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <div class="row">
                	<div class="col-md-12">                		
						<div class="table-responsive">
				            <table id="table-user" class="table table-bordered table-striped">
				                <thead>
				                    <tr>
				                        <th>No</th>
					        			<th>Nama</th>
					        			<th>Username</th>
					        			<th>Email</th>
					        			<th>Role</th>
					        			<th>Status</th>
					        			<th>Aksi</th>
				                    </tr>
				                </thead>
				                
				            </table>
				        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $('.deleteall').click(function(){
        var url    = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(0).val());
        var alert1 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(1).val());
        var alert2 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(2).val());
        var alert3 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(3).val());
        var alert4 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(4).val());

        swal({
            // title: alert1,
            text: alert1,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = [];

                $(this).parent().parent().parent().find('tbody :checkbox:checked').each(function(i){
                    id[i] = $(this).val();
                });

                if ( id.length === 0) {
            
                    swal(alert2, {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
            
                } else {
            
                    $.ajax({
                        url: '<?php echo base_url(); ?>' + url,
                        method:'POST',
                        data:{id:id},
                        success:function(data) {
                            if (data == "success") {
                                for ( var i = 0; i < id.length; i++ ) {
                                    $('tr#id-'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#id-'+id[i]+'').fadeOut('slow');
                                }

                                swal(alert3, {
                                    icon: "success",
                                    buttons: false,
                                    timer: 2000,
                                });
                            } else {
                                swal(alert4, {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000,
                                });
                            }
                        }
                    });
                }
            }
        });
    });
</script>