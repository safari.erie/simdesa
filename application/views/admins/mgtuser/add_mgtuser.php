<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>admins/MgtUser">
                List User
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                Add User
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">          
                <?php
                    echo alert()
                ?>      
                <form method="post" role="form"  
                    id="user-form" action="" autocomplete="off">
                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">
                            Role <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="role" id="role" class="select2 form-control" onchange="submit()" required>
                            <option value=""> --- Pilih --- </option>
                                <?php
                                    foreach ($role as $index => $val) {                                         
                                        if ($post['role'] == $val->id_role) {
                                            $selected1 = "selected";
                                        } else {
                                            $selected1 = "";
                                        }                                              
                                ?>
                                    <option value="<?php echo $val->id_role; ?>" <?php echo $selected1;?>>
                                        <?php echo $val->role_name; ?>
                                    </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <?php if ( @$post['role'] == 3 ) {?>
                        <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">
                            Kecamatan <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="kecamatan" id="kecamatan" class="select2 form-control"  required>
                            <option value=""> --- Pilih --- </option>
                                <?php
                                    foreach ($list_kecamatan as $data) {
                                        if ($post['kode_kec'] == $data['kode_kec']) {
                                            $selected1 = "selected";
                                        } else {
                                            $selected1 = "";
                                        }
                                ?>
                                    <option value="<?php echo $data['kode_kec']; ?>" <?php echo $selected1; ?>>
                                        <?php echo $data['name']; ?>
                                    </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        </div>
                    <?php } ?>

                    <?php if ( @$post['role'] == 4 ) {?>
                        <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">
                            Desa <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="desa" id="desa" class="select2 form-control"  required>
                            <option value=""> --- Pilih --- </option>
                                <?php
                                    foreach ($list_desa as $data) {
                                        if ($post['kode_desa'] == $data['kode_desa']) {
                                            $selected1 = "selected";
                                        } else {
                                            $selected1 = "";
                                        }
                                ?>
                                    <option value="<?php echo $data['kode_desa']; ?>" <?php echo $selected1; ?>>
                                        <?php echo $data['name']; ?>
                                    </option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        </div>
                    <?php } ?>

                  
                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">
                            Username <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-3">                            
                            <input type="text" class="form-control" name="username" placeholder="Username ..."  required>
                        </div>
                    </div>    
                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">
                            Password <span class="text-danger">*</span>
                        </label>
                        <div class="col-md-3">
                            <input type="password" minlength="6" class="form-control" name="password" placeholder="Password ..."  required>
                        </div>
                        
                    </div>

                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">Email:</label>
                        <div class="col-sm-9">
							<input type="email" class="form-control" name="email" id="email" data-input="wajib" placeholder="Ketikkan Email..." required />
							<div class="invalid-feedback">Email tidak boleh kosong</div>
						</div>
                    </div>
                    
                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">
                            Nama Lengkap 
                        </label>
                        <input type="text" class="form-control col-md-3 mr-2" name="firstName" id="firstName" placeholder="Nama Depan..." />
                        <input type="text" class="form-control col-md-3 mr-2" name="middleName" id="middleName" placeholder="Nama Tengah..." />
                        <input type="text" class="form-control col-md-3 mr-2" name="lastName" id="lastName" placeholder="Nama Belakang..." />
                        
                    </div>
                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">Alamat Lengkap:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="address" id="address" placeholder="Ketikkan Alamat Lengkap..."></textarea>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">Nomor Telpon:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="PhoneNumber" id="phoneNumber" placeholder="Ketikkan Nomor Telpon..." />
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="control-label mt-2 col-md-2">Nomor HP:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="MobileNumber" id="mobileNumber" placeholder="Ketikkan Nomor HP..." />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" id="btn_submit" class="btn btn-success" onclick="submit_user()"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

