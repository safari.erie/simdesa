<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>admins/MgtRab">
                List RAB
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                Add RAB
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <a class="btn btn-info" href="javascript:void(0)" onclick="showModalProposal()">
                    Pilih Proposal
                </a> 
                <div id="form_rab" style="display:none">
                    <form method="post" role="form" id="rent-form" autocomplete="off">
                    <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover nowrap myTableRab" id="myTableDiklat">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Item Rab</th>
                                        <th>Nilai RAB</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>                              
                            </table>
                        </div>
                        <button type="button" onclick="AddRab();" class="btn btn-info btn-block mb-4"><i class="fa fa-plus"></i> Tambah Rab</button>               
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_proposal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Daftar Proposal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="dataProposal" class="table table-condensed table-bordered table-colored table-custom m-0" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Proposal</th>
                    <th>Kecamatan</th>
                    <th>Desa</th>
                    <th>Nilai Proposal</th>					
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="javascript:void(0)" class=""> 1 </a></td>
                    <td>Pembangunan Jalan 1</td>
                    <td>Babakn madang</td>
                    <td>Babakn madang</td>
                    <td>500.000.000</td>
                </tr>
                <tr>
                    <td><a href="javascript:void(0)" class=""> 2 </a></td>
                    <td>Pembangunan Jalan 2</td>
                    <td>Babakn madang</td>
                    <td>Babakn madang</td>
                    <td>500.000.000</td>
                </tr>
            </tbody>
        </table>
      </div>      
    </div>
  </div>
</div>



