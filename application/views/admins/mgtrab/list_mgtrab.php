<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                List RAB
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>admins/MgtRab/Add">
                Add RAB
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <div class="row">
                	<div class="col-md-12">
                		<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0)" class="btn btn-danger deleteall">
                                    <i class="fa fa-times"></i> <?php echo 'Hapus' ?>
                                </a>
							</div>
						</div>
						<div class="table-responsive">						
	                		<input type="hidden" value="<?php echo base64_encode('admins/mgtkecamatan/delete'); ?>">
				            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_choose_one')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_success')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_failed')); ?>">

				            <table id="table-rentist" class="table table-bordered table-striped">
				                <thead>
				                    <tr>
				                        <th>#</th>
					        			<th>Nama RAB</th>
					        			<th>Nilai RAB</th>
					        			<th>Kecamatan</th>
					        			<th>Desa</th>
					        			<th>#</th>
				                    </tr>
				                </thead>
				                
				            </table>
				        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  
</script>