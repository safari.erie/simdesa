<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<style>
    .hscroll {
        width: 100%;
        overflow-x: auto;
        white-space: nowrap;
    }
</style>

<!-- $content -->
<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <center>
            <h4 class="text-themecolor"><strong>SELAMAT DATANG DI APLIKASI MONITORING DESA</strong></h4>
        </center>
    </div>
    
</div>

<div class="card">
    <div class="card-body collapse show">
        <div class="card">
            <div class="card-header">
                <strong>Filter Data:</strong>
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    Kecamatan
                                </label>
                                <select id="kec_id" name="kec_id" class="select2 form-control" <?php echo @$post['kec_id_disabled']; ?>>
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_kecamatan as $data) {
                                            if ($post['kec_id'] == $data['id']) {
                                                $selected1 = "selected";
                                            } else {
                                                $selected1 = "";
                                            }
                                    ?>
                                        <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                            <?php echo $data['nama']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    Desa
                                </label>
                                <select id="desa_id" name="desa_id" class="select2 form-control" <?php echo @$post['desa_id_disabled']; ?>>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin: auto;">
                            <div class="form-actions pull-left" style="margin-right:10px;">
                                <button type="submit" id="filter" name="filter" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> Filter
                                </button>
                            </div>


                            <div class="form-actions pull-left">
                                <button type="submit" id="export" name="export" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> Export Data
                                </button>
                            </div>
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

