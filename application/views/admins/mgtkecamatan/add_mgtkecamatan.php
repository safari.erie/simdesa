<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>admins/MgtKecamatan">
                List Kecamatan
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                Add kecamatan
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
               
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-6 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Zonasi Kecamatan <span class="text-danger">*</span>
                                </label>
                                <input type="number" class="form-control" name="zona_kec" value="" required>
                            </div>
                        </div>
                        <div class="col-md-6 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    Kecamatan <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="nama" value="" required>
                            </div>
                        </div>

                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

