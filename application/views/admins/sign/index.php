<section id="wrapper">
    <div class="login-register" style="background-image:url('<?php echo base_url(); ?>assets/images/bg.jpg');">
        <div class="login-box card" style="box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.3)">
            <div style="padding-top: 20px">
                <center><strong><h3>SAMISADE</h3></strong></center>
            </div>
            <div class="card-body">
                <form method="post" action="<?php echo base_url('admins/sign/check_login')?>"
                 role="form" id="sign-form" class="form-horizontal form-material">
                
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required
                             placeholder="Username" name="username" id="username"> 
                             <?php if ($this->session->flashdata('notif_email_wrong') != 'success'): ?>                          
                                <div class="form-group">
                                        <div class="d-flex justify-content-between mg-b-5">
                                        <span class="tx-13 text-danger"><?php echo $this->session->flashdata('notif_email_wrong'); ?></span>
                                        </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required 
                            placeholder="Password" name="password" id="password"> 
                            <?php if ($this->session->flashdata('notif_pass_wrong') != 'success'): ?>                          
                                <div class="form-group">
                                        <div class="d-flex justify-content-between mg-b-5">
                                        <span class="tx-13 text-danger"><?php echo $this->session->flashdata('notif_pass_wrong'); ?></span>
                                        </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            
                            <button class="btn btn-block btn-lg btn-success" id="btn-sign" type="submit" onclick="submit_login()">
                                Login
                            </button>
                        </div>
                    </div>


                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            @copyright 2021
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>